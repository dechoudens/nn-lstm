import os
import re
import unidecode
import tkinter
from tkinter import filedialog

def processfile(filePath):
    file = open(filePath, encoding="utf8")
    mystring = file.read().replace('\n', '')
    nolink_text = re.sub(r'^https?:\/\/.*[\r\n]*', '', mystring)
    unaccented_string = unidecode.unidecode(nolink_text)

    return re.sub("[^A-Za-z0-9,',.:]+", ' ', unaccented_string).lower()

def getWeightsFile():
    root = tkinter.Tk()
    root.withdraw()
    return filedialog.askopenfilename(initialdir=os.getcwd() + "/Weights", title="Select weights file", filetypes=[("hdf5 Files", "*.hdf5")])

def getDataFile():
    root = tkinter.Tk()
    root.withdraw()
    return filedialog.askopenfilename(initialdir=os.getcwd() + "/Files", title="Select data file", filetypes=[("Text Files", "*.txt")])
